﻿using System;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace LostPolygon {
    public static class Shell {
        [MenuItem("Tools/Start shell client")]
        public static void StartShell() {
            Debug.Log("STARTING SHELL");
            foreach (Assembly assembly in System.AppDomain.CurrentDomain.GetAssemblies())
            {
                Debug.Log(assembly.FullName + " @ " + assembly.Location);
            }

            try
            {
                new SimpleShell.Shell("lostpolygon.com", 47658).Connect();
            } catch (Exception e)
            {
                Debug.LogException(e);
                throw;
            }
        }

#if UNITY_CLOUD_BUILD
        public static void PreExport(UnityEngine.CloudBuild.BuildManifestObject manifest) {
            StartShell();
        }
#endif
    }
}
